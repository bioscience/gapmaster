from snakemake.shell import shell

from pyfaidx import Fasta

scaffolds = snakemake.input.get("scafs")
coords = snakemake.input.get("coords")
mpileup = snakemake.input.get("mpileup")
agp = snakemake.input.get("agp")

flank = int(snakemake.params.get("flank", ""))
repeatShare = float(snakemake.params.get("repeatShare", ""))
readType = snakemake.params.get("readtype", "")

# Create mapping back to original scaffolds
dCoords = {}
scafId = None
with open(coords) as handle: # First remap coordinate changes
    for line in handle:
        items = line.strip().split('\t')
        if line[0] == ">":
            scafId = line.strip()[1:]
            continue
        if len(items) == 5 and items[2] == 'S':
            dCoords["%s_%s_%s" %(scafId, items[3], items[4])] = "%s_%s_%s" %(scafId, items[0], items[1])

dMap = {}
with open(agp) as handle: # Second map scaffolds based on remapped coordinates
    for line in handle:
        items = line.strip().split('\t')
        if len(items) == 9 and items[4] == "W":
            try:
                dMap[dCoords["%s_%s_%s" %(items[0], items[1], items[2])]] = items[5]         
            except KeyError:
                pass # tgsGapCloser has a bug with coordinates that leads to a keyerror

#>grep scf00039 WorkDir/ragtag.merge.agp
#scf00039_RagTag 1       267086  1       W       contig_2544_segment0_pilon      1       267086  +
#scf00039_RagTag 267087  331588  2       N       64502   scaffold        yes     align_genus
#scf00039_RagTag 331589  432714  3       W       contig_7519_segment0_pilon      1       101126  -
#scf00039_RagTag 432715  432814  4       U       100     scaffold        yes     align_genus
#scf00039_RagTag 432815  572725  5       W       contig_10317_segment0_pilon     1       139911  -
#scf00039_RagTag 572726  572825  6       U       100     scaffold        yes     align_genus
#scf00039_RagTag 572826  723072  7       W       contig_12171_segment0_pilon     1       150247  +
#scf00039_RagTag 723073  751431  8       N       28359   scaffold        yes     align_genus
#scf00039_RagTag 751432  922239  9       W       scaffold_31993_segment0_pilon   1       170808  +
#
#>grep -A10 scf00039 WorkDir/tgsNanopore.gap_fill_detail
#>scf00039_RagTag
#1       267086  S       1       267086
#267087  331588  N
#331589  432714  S       331589  432714
#432715  439889  F
#439890  579800  S       432815  572725
#579801  579900  N
#579901  730147  S       572826  723072
#730148  758506  N
#758507  929314  S       751432  922239

dLens, dScafs, scafId = {}, {}, None
with open(coords) as handle:
    for line in handle:
        if line[0] == ">":
            scafId = line.strip()[1:]
            dLens[scafId], dScafs[scafId] = 0, []
            continue
        items = line.strip().split('\t')
        dLens[scafId] = int(items[1])
        #print (items)
with open(coords) as handle:
    for line in handle:
        if line[0] == ">":
            scafId = line.strip()[1:]
            continue
        items = line.strip().split('\t')
        if items[2] == "F":
            fs, fe = int(items[0]), int(items[1])
            s, e = fs - flank, fe + flank
            if s < 1: s = 1
            if e > dLens[scafId]: e = dLens[scafId]
            dScafs[scafId].append((s, e, fs, fe))

coordinates, allCoords = [], []
borderCoords, borders = [], []
dBorders = {}
for scafId in dScafs:
    #dBorders[scafId] = []
    for s, e, fs, fe in dScafs[scafId]:
        coordinates.append("%s\t%d\t%d" %(scafId, s, e))
        allCoords.append("%s\t%d\t%d\t%d\t%d" %(scafId, s, e, fs, fe))
        borderCoords.append("%s\t%d\t%d" %(scafId, s - 1, fs + flank - 1)) # 0-based for mpileup
        borderCoords.append("%s\t%d\t%d" %(scafId, fe - flank - 1, e - 1)) # 0-based for mpileup
        #print ("%s_%d" %(scafId, s))
        dBorders["%s_%d" %(scafId, s)] = s, fs + flank, fe - flank, e

#log = snakemake.log_fmt_shell()
with open(snakemake.log[0], 'w') as logHandle:
    dPile = {}
    key = None
    with open(mpileup) as handle:
        for line in handle:
            scafId, pos, refNt, depth, reads = line.strip().split('\t')
            pos, depth = int(pos), int(depth)
            readStarts, readEnds = reads.count('^'), reads.count('$')
            try:
                s, bs, be, e = dBorders["%s_%d" %(scafId, pos)]
                key = "%s_%d" %(scafId, bs - flank)
            except KeyError:
                pass
            try:
                dPile[key].append((pos, depth, readStarts, readEnds, reads, refNt))
            except KeyError:
                dPile[key] = [] 
                dPile[key].append((pos, depth, readStarts, readEnds, reads, refNt))
    
    dClosedGaps = {}
    for key in dPile:
        c, s = 0, 0
        cnt = 0
        broken = False
        isBroken = False
        repCnt = 0
        for i in range(len(dPile[key])):
            item = dPile[key][i]
            item = [j for j in item]
            pos, depth, readStarts, readEnds, reads, refNt = item
            item.pop(-2)
            print ('_'.join(key.split('_')[:-1]), '\t'.join([str(i) for i in item]), file=logHandle)
            if readEnds >= depth or depth == 0: broken = True
            if refNt.islower() == True: repCnt += 1
            s += depth
            c += readStarts + readEnds
            cnt += 1
            if cnt % (2 * flank) == 0 or i >= len(dPile[key]) - 1:
                avgDepth = float(s) / float(2 * flank)
                breakPoint = True
                if broken == False and avgDepth > 1.0 and float(c) < avgDepth: breakPoint = False
                # Half are repeats i.e. break because not reliable
                if float(repCnt) / float(cnt) > repeatShare: breakPoint = True
                print ("", file=logHandle)
                print ("#", breakPoint, s / float(2 * flank), c, float(repCnt) / float(cnt), file=logHandle)
                #print ("", file=logHandle)
                if breakPoint == True:
                    isBroken = True
                    break
                s, c = 0, 0
                cnt, repCnt = 0, 0
        print ("", file=logHandle)
        scafId = '_'.join(key.split('_')[:-1])
        pos = int(key.split('_')[-1])
        print ("### %s\t%d\t%s" %(scafId, pos, isBroken), file=logHandle)
        print ("", file=logHandle)
        if isBroken == False:
            dClosedGaps[key] = True
    
    cnt = 1
    scafs = Fasta(scaffolds)
    start, breakScaf = None, False
    #handleE = open("test.err", 'w')
    with open(snakemake.output[0], 'w') as handleW:
        with open(coords) as handle:
            for line in handle:
                if line[0] == ">":
                    if start != None:
                        #handleW.write(">Scaf_%d\n" %cnt)
                        try:
                            handleW.write(">%s\n" %dMap["%s_%s_%s" %(scafId, start, end)])
                        except KeyError:
                            handleW.write(">%s_%s_%s\n" %(scafId, start, end))
                        handleW.write("%s\n" %scafs[scafId][start-1:end].seq)
                        #if scafId == "scf00039_RagTag":
                        #    handleE.write("%s %d - %d\n" %(scafId, start, end))
                        cnt += 1
                    scafId = line.strip()[1:]
                    start, breakScaf = None, False
                    continue
                items = line.strip().split('\t')
                s, e, qual = int(items[0]), int(items[1]), items[2]
                if start == None: start = s
                if qual == 'S': end = e
                if qual == 'N': breakScaf = True
                if qual == 'F':
                    try:
                        closed = dClosedGaps["%s_%s" %(scafId, s)]
                        end = e
                    except KeyError:
                        breakScaf = True
                if breakScaf == True:
                    #handleW.write(">Scaf_%d\n" %cnt)
                    try:
                        handleW.write(">%s\n" %dMap["%s_%s_%s" %(scafId, start, end)])
                    except KeyError:
                        handleW.write(">%s_%s_%s\n" %(scafId, start, end))
                    handleW.write("%s\n" %scafs[scafId][start-1:end].seq)
                    #if scafId == "scf00039_RagTag":
                    #    handleE.write("%s %d - %d\n" %(scafId, start, end))
                    cnt += 1
                    start = None
                    breakScaf = False
        if start != None:
            #handleW.write(">Scaf_%d\n" %cnt)
            try:
                handleW.write(">%s\n" %dMap["%s_%s_%s" %(scafId, start, end)])
            except KeyError:
                handleW.write(">%s_%s_%s\n" %(scafId, start, end))
            handleW.write("%s\n" %scafs[scafId][start-1:end].seq)
            #if scafId == "scf00039_RagTag":
            #    handleE.write("%s %d - %d\n" %(scafId, start, end))
