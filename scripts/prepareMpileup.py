from snakemake.shell import shell

#scafs = snakemake.input.get("scafs")
coords = snakemake.input.get("coords")
#bam = snakemake.input.get("bam")

flank = int(snakemake.params.get("flank", ""))
#repeatShare = snakemake.params.get("repeatShare", "")
readType = snakemake.params.get("readtype", "")

log = snakemake.log_fmt_shell()

dLens, dScafs, scafId = {}, {}, None
with open(coords) as handle:
    for line in handle:
        if line[0] == ">":
            scafId = line.strip()[1:]
            dLens[scafId], dScafs[scafId] = 0, []
            continue
        items = line.strip().split('\t')
        dLens[scafId] = int(items[1])
        #print (items)
with open(coords) as handle:
    for line in handle:
        if line[0] == ">":
            scafId = line.strip()[1:]
            continue
        items = line.strip().split('\t')
        if items[2] == "F":
            fs, fe = int(items[0]), int(items[1])
            s, e = fs - flank, fe + flank
            if s < 1: s = 1
            if e > dLens[scafId]: e = dLens[scafId]
            dScafs[scafId].append((s, e, fs, fe))

coords, allCoords = [], []
borderCoords, borders = [], []
dBorders = {}
for scafId in dScafs:
    #dBorders[scafId] = []
    for s, e, fs, fe in dScafs[scafId]:
        coords.append("%s\t%d\t%d" %(scafId, s, e))
        allCoords.append("%s\t%d\t%d\t%d\t%d" %(scafId, s, e, fs, fe))
        borderCoords.append("%s\t%d\t%d" %(scafId, s - 1, fs + flank - 1)) # 0-based for mpileup
        borderCoords.append("%s\t%d\t%d" %(scafId, fe - flank - 1, e - 1)) # 0-based for mpileup
        #print ("%s_%d" %(scafId, s))
        dBorders["%s_%d" %(scafId, s)] = s, fs + flank, fe - flank, e
with open(snakemake.output[0], 'w') as handle:
    print ("%s" %'\n'.join(borderCoords), file=handle)
    #print ("%s\n" %'\n'.join(coords), file=handle)
with open(snakemake.output[1], 'w') as handle:
    print ("%s" %'\n'.join(allCoords), file=handle)
