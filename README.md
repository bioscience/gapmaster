# GapMaster

GapMaster scaffolds given contigs based on reference genome(s) and closes the resultant gaps that can be fully validated using either PacBio or Nanopore long reads.