rule mpileupPacBio:
    input:
        scafs="{basename}/tgsPacBio.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        bam="{basename}/pacBio.bam".format(basename=os.path.basename(config["workDir"])),
        positions="{basename}/possPacBio.txt".format(basename=os.path.basename(config["workDir"])),
        #bai="{basename}/pacBio.bam.bai".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/tgsPacBio.mpileup".format(basename=os.path.basename(config["workDir"])),
    conda:
        "../envs/samtools.yaml"
    log:
        "logs/mpileup/pacbio.log"
    #params:
    #    extra=""
    #threads: workflow.cores
    shell:
        """samtools view -F 0x904 -h -b {input.bam} | samtools mpileup -f {input.scafs} -aa - -l {input.positions} | awk -F"\t" '{{print $1"\t"$2"\t"$3"\t"$4"\t"$5}}' > {output}"""
