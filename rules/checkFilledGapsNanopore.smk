rule checkFilledGapsNanopore:
    input:
        scafs="{basename}/tgsNanopore.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        coords="{basename}/tgsNanopore.gap_fill_detail".format(basename=os.path.basename(config["workDir"])),
        mpileup="{basename}/tgsNanopore.mpileup".format(basename=os.path.basename(config["workDir"])),
        agp="{basename}/ragtag.merge.agp".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/gapMasterNanopore.fna".format(basename=os.path.basename(config["workDir"])),
    #conda:
    #    "../envs/pyfaidx.yaml"
    log:
        "logs/gapmaster/check.log"
    params:
        flank=config["flank"],
        repeatShare=config["repeatShare"],
        readType="Nanopore"
    #threads: workflow.cores
    script:
        "../scripts/checkFilledGaps.py"
