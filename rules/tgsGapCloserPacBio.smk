pacBioReads = []
try:
    if config["longReadData"]["pacBio"]:
        pacBioReads.append(config["longReadData"]["pacBio"])
except KeyError:
    pass

rule tgsGapCloserPacBio:
    input:
        contigs="{basename}/ragtag.merge.fasta".format(basename=os.path.basename(config["workDir"])),
        reads=pacBioReads
    output:
        scafs="{basename}/tgsPacBio.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        coords="{basename}/tgsPacBio.gap_fill_detail".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/tgsgapcloser.yaml"
    log:
        "logs/tgsgapcloser/tgsgapcloserpacbio.log"
    params:
        prefix="{basename}/tgsPacBio".format(basename=os.path.basename(config["workDir"])),
        minIdy=config["tgsGapCloser"]["minIdy"],
        minMatch=config["tgsGapCloser"]["minMatch"]
    threads: workflow.cores
    shell:
        "(tgsgapcloser --tgstype pb --min_idy {params.minIdy} --min_match {params.minMatch} --scaff {input.contigs} --reads {input.reads} --ne --thread {threads} --output {params.prefix}) > {log}"
