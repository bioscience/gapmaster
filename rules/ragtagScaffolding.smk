rule ragtagScaffolding:
    input:
        reference=lambda wildcards: config["references"][wildcards.ref],
        contigs=config["contigs"]
    output:
        scafs="{basename}/{{ref}}/ragtag.scaffolds.agp".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/ragtag.yaml"
    log:
        "logs/tgsgapcloser/tgsgapclosernanopore{ref}.log"
    params:
        wd=config["workDir"]
    threads: workflow.cores
    shell:
        "(ragtag.py scaffold {input.reference} {input.contigs} -r -g 1 -o {params.wd}/{wildcards.ref} -u -t {threads}) > {log}"

