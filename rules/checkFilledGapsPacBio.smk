rule checkFilledGapsPacBio:
    input:
        scafs="{basename}/tgsPacBio.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        coords="{basename}/tgsPacBio.gap_fill_detail".format(basename=os.path.basename(config["workDir"])),
        mpileup="{basename}/tgsPacBio.mpileup".format(basename=os.path.basename(config["workDir"])),
        agp="{basename}/ragtag.merge.agp".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/gapMasterPacBio.fna".format(basename=os.path.basename(config["workDir"])),
    #conda:
    #    "../envs/pyfaidx.yaml"
    log:
        "logs/gapmaster/check.log"
    params:
        flank=config["flank"],
        repeatShare=config["repeatShare"],
        readType="PacBio"
    #threads: workflow.cores
    script:
        "../scripts/checkFilledGaps.py"
