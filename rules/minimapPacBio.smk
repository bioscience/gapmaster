pacBioReads = []
try:
    if config["longReadData"]["pacBio"]:
        pacBioReads.append(config["longReadData"]["pacBio"])
except KeyError:
    pass

rule minimapPacBio:
    input:
        scafs="{basename}/tgsPacBio.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        reads=pacBioReads
    output:
        bam="{basename}/pacBio.bam".format(basename=os.path.basename(config["workDir"])),
        bai="{basename}/pacBio.bam.bai".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/minimap2.yaml"
    log:
        "logs/minimap/pacbio.log"
    #params:
    #    extra=""
    threads: workflow.cores
    shell:
        "minimap2 -H -x map-pb -a -t {threads} {input.scafs} {input.reads} | samtools view -@ {threads} -b - | samtools sort -@ {threads} > {output.bam} && "
        "bamtools index -in {output.bam}"
