nanoporeReads = []
try:
    if config["longReadData"]["nanopore"]:
        nanoporeReads.append(config["longReadData"]["nanopore"])
except KeyError:
    pass

rule minimapNanopore:
    input:
        scafs="{basename}/tgsNanopore.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        reads=nanoporeReads
    output:
        bam="{basename}/nanopore.bam".format(basename=os.path.basename(config["workDir"])),
        bai="{basename}/nanopore.bam.bai".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/minimap2.yaml"
    log:
        "logs/minimap2/nanopore.log"
    #params:
    #    extra=""
    threads: workflow.cores
    shell:
        "minimap2 -H -x map-ont -a -t {threads} {input.scafs} {input.reads} | samtools view -@ {threads} -b - | samtools sort -@ {threads} > {output.bam} && "
        "bamtools index -in {output.bam}"
