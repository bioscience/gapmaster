rule prepareMpileupNanopore:
    input:
        coords="{basename}/tgsNanopore.gap_fill_detail".format(basename=os.path.basename(config["workDir"]))
    output:
        positions="{basename}/possNanopore.txt".format(basename=os.path.basename(config["workDir"])),
        allPositions="{basename}/allPossNanopore.txt".format(basename=os.path.basename(config["workDir"]))
    #conda:
    #    "../envs/pyfaidx.yaml"
    log:
        "logs/preparempileup/preparempileup.log"
    params:
        flank=config["flank"],
        #repeatShare=config["repeatShare"],
        readType="Nanopore"
    #threads: workflow.cores
    script:
        "../scripts/prepareMpileup.py"
