rule prepareMpileupPacBio:
    input:
        coords="{basename}/tgsPacBio.gap_fill_detail".format(basename=os.path.basename(config["workDir"]))
    output:
        positions="{basename}/possPacBio.txt".format(basename=os.path.basename(config["workDir"])),
        allPositions="{basename}/allPossPacBio.txt".format(basename=os.path.basename(config["workDir"]))
    #conda:
    #    "../envs/pyfaidx.yaml"
    log:
        "logs/preparempileup/preparempileup.log"
    params:
        flank=config["flank"],
        #repeatShare=config["repeatShare"],
        readType="PacBio"
    #threads: workflow.cores
    script:
        "../scripts/prepareMpileup.py"
