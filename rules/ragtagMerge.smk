rule ragtagMerge:
    input:
        #references=expand(config["references"]),
        references=expand("{basename}/{{ref}}/ragtag.scaffolds.agp".format(basename=os.path.basename(config["workDir"])), ref=config["references"]),
        contigs=config["contigs"]
    output:
        scafs="{basename}/ragtag.merge.fasta".format(basename=os.path.basename(config["workDir"])),
        agp="{basename}/ragtag.merge.agp".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/ragtag.yaml"
    log:
        "logs/tgsgapcloser/tgsgapclosernanopore.log"
    params:
        wd=config["workDir"]
    #threads: workflow.cores
    shell:
        "cnt=`ls {params.wd}/*/ragtag.scaffolds.agp | wc -l | awk '{{print $1}}'` && "
        "echo $cnt && "
        "if [ $cnt -eq 1 ]; then cp {params.wd}/*/ragtag.scaffolds.agp {params.wd}/ragtag.merge.agp; cp {params.wd}/*/ragtag.scaffolds.fasta {params.wd}/ragtag.merge.fasta; else "
        "(ragtag.py merge -u {input.contigs} {params.wd}/*/ragtag.scaffolds.agp -o {params.wd}) > {log}; fi"

