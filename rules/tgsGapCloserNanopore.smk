nanoporeReads = []
try:
    if config["longReadData"]["nanopore"]:
        nanoporeReads.append(config["longReadData"]["nanopore"])
except KeyError:
    pass

rule tgsGapCloserNanopore:
    input:
        contigs="{basename}/ragtag.merge.fasta".format(basename=os.path.basename(config["workDir"])),
        reads=nanoporeReads
    output:
        scafs="{basename}/tgsNanopore.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        coords="{basename}/tgsNanopore.gap_fill_detail".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/tgsgapcloser.yaml"
    log:
        "logs/tgsgapcloser/tgsgapclosernanopore.log"
    params:
        prefix="{basename}/tgsNanopore".format(basename=os.path.basename(config["workDir"])),
        minIdy=config["tgsGapCloser"]["minIdy"],
        minMatch=config["tgsGapCloser"]["minMatch"]
    threads: workflow.cores
    shell:
        "(tgsgapcloser --tgstype ont --min_idy {params.minIdy} --min_match {params.minMatch} --scaff {input.contigs} --reads {input.reads} --ne --thread {threads} --output {params.prefix}) > {log}"
