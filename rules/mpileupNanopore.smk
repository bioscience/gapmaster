rule mpileupNanopore:
    input:
        scafs="{basename}/tgsNanopore.scaff_seqs".format(basename=os.path.basename(config["workDir"])),
        bam="{basename}/nanopore.bam".format(basename=os.path.basename(config["workDir"])),
        positions="{basename}/possNanopore.txt".format(basename=os.path.basename(config["workDir"])),
        #bai="{basename}/nanopore.bam.bai".format(basename=os.path.basename(config["workDir"]))
    output:
        "{basename}/tgsNanopore.mpileup".format(basename=os.path.basename(config["workDir"]))
    conda:
        "../envs/samtools.yaml"
    log:
        "logs/mpileup/nanopore.log"
    #params:
    #    extra=""
    #threads: workflow.cores
    shell:
        """samtools view -F 0x904 -h -b {input.bam} | samtools mpileup -f {input.scafs} -aa - -l {input.positions} | awk -F"\t" '{{print $1"\t"$2"\t"$3"\t"$4"\t"$5}}' > {output}"""
