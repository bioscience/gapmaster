###########################################
# Preprocessing the read data
###########################################

import os
import pandas as pd
from snakemake.utils import validate, min_version
##### set minimum snakemake version #####
min_version("5.1.2")

configfile: "config.yaml"
#validate(config, schema="schemas/config.schema.yaml")

#ragtag.py scaffold ../Refs/EG_G1.human.chr.fa chr.rn.fna -r -g 1 -o chrA -u -t 24
#mkdir -p Tgs2
#tgsgapcloser --tgstype pb --min_idy 0.05 --min_match 1000 --scaff chrA/ragtag.scaffolds.fasta --reads ../Reads/Eco/eco.trimmedReads.fasta.gz --ne --thread 72 --output Tgs2/tgs
## Tss2/tgs.scaff_seqs are copied directly to eco.scafs.fna
#python ../checkFills.py -i Tgs2/tgs.scaff_seqs -c Tgs2/tgs.gap_fill_detail -r ../Reads/Eco/eco.correctedReads.fasta.gz -T 72 > Tgs2/tgs.clean.fna
## Remove chromosomes 1 and 8 from eco.scafs1.fna and copy Tgs1/tgs.clean.fna to eco.scafs2a.fna
#python ~/Codebase/divideToContigs.py -i eco.scafs2a.fna -n 0 > eco.contigs2.fna
#ragtag.py scaffold ../Refs/EG_G1.human.chr.fa eco.contigs2.fna -r -g 1 -o ChrA -u -t 24

include: "rules/ragtagScaffolding.smk"
include: "rules/ragtagMerge.smk"
include: "rules/tgsGapCloserPacBio.smk"
include: "rules/tgsGapCloserNanopore.smk"
include: "rules/prepareMpileupPacBio.smk"
include: "rules/prepareMpileupNanopore.smk"
include: "rules/minimapPacBio.smk"
include: "rules/minimapNanopore.smk"
include: "rules/mpileupPacBio.smk"
include: "rules/mpileupNanopore.smk"
include: "rules/checkFilledGapsPacBio.smk"
include: "rules/checkFilledGapsNanopore.smk"

rule all:
    input:
        #"WorkDir/gapMasterPacBio.fna",
        "WorkDir/gapMasterNanopore.fna",
        #"WorkDir/pacBio.bam",
        #"WorkDir/nanopore.bam",
        #"WorkDir/gapMasterPacBio.fna",
        #"WorkDir/gapMasterNanopore.fna",

#        "GenePreds.all.gff",
#        "GenePreds.all.maker.transcripts.fasta",
#        "GenePreds.all.maker.proteins.fasta",
#        "trinityGg/Trinity-GG.fasta",
#        expand("trimmed/{sample}-{unit}.{group}.fq.gz", group=[1,2], sample="sample1", unit=units.loc["sample1", "unit"]),
#        "index/{genomeBase}".format(genomeBase=os.path.basename(config["reference"])),
#        expand("mapped/{genomeBase}-{sample}-{unit}.bam", genomeBase=os.path.basename(config["reference"]), sample="sample1", unit=units.loc[:,"unit"]),
#        "mapped/{genomeBase}.bam".format(genomeBase=os.path.basename(config["reference"])),
#        "trinityGg/Trinity-GG.fasta",
#         units.loc[(wildcards.sample, wildcards.unit), ["fastq1", "fastq2"]].dropna()
#         getFastqRna
#        expand("trimmed/{sample}-{unit}.1.fastq.gz", units.index),
#        expand("trimmed/{sample}-{unit}.1.fastq.gz", units.index),
#        expand("trimmed/{sample}-{unit}.{group}.fastq.gz", group=[1, 2], **wildcards)
#        "mapped/{genomeBase}.bam",
#        #"trinityDn/Trinity.fasta",
#        #lambda wildcards: config["leftReadFiles"][wildcards.leftReadFileId]),
#        #lambda wildcards: config["rightReadFiles"][wildcards.rightReadFileId])
